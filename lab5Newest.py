#assessment of the actual division of labor on the assignment: We worked on this together: pair programming for the win!!






from doctest import testmod

######################################## 1

class Fib():
       """A Fibonacci number.
       >>> start = Fib()
       >>> start
       0
       >>> start.next()
       1
       >>> start.next().next()
       1
       >>> start.next().next().next()
       2
       >>> start.next().next().next().next()
       3
       >>> start.next().next().next().next().next()
       5
       >>> start.next().next().next().next().next().next()
       8
       >>> start.next().next().next().next().next().next() # Ensure start isn’t changed
       8
       """
       def __init__(self, value=0):
           self.value = value
       def next(self):
           if self.value == 0:
               final = Fib(1)
           else:
               final = Fib(self.value + self.previous)
           final.previous = self.value
           return final
       def __repr__(self):
           return str(self.value)


start = Fib()
#print(start)
#print(start.next())
#print(start.next().next())
#print(start.next().next().next())
#print(start.next().next().next().next())
#print(start.next().next().next().next().next())
#print(start.next().next().next().next().next().next())



######################################## 2


def vending_machine(snacks):
      """
      Cycles through sequence of snacks.
      >>> vender = vending_machine(('chips', 'chocolate', 'popcorn'))
      >>> vender()
      'chips'
      >>> vender()
      'chocolate'
      >>> vender()
      'popcorn'
      >>> vender()
      'chips'
      >>> other = vending_machine(('brownie',))
      >>> other()
      'brownie'
      >>> vender()
      'chocolate'
      """
      def func():
          nonlocal snacks
          snacks = list(snacks)
          first_element = snacks[0]
          snacks = snacks[1:]
          snacks.append(first_element)
          return first_element
      return func

vender = vending_machine(('chips', 'chocolate', 'popcorn'))
#print(vender())
#print(vender())
#print(vender())
#print(vender())

#print()
other = vending_machine(('brownie',))
#print(other())
#print(vender())


######################################## 3

class VendingMachine:
      """A vending machine that vends some product for some price.
      >>> v = VendingMachine('candy', 10)
      >>> v.vend()
      'Machine is out of stock.'
      >>> v.deposit(15)
      'Machine is out of stock. Here is your $15.'
      >>> v.restock(2)
      'Current candy stock: 2'
      >>> v.vend()
      'You must deposit $10 more.'
      >>> v.deposit(7)
      'Current balance: $7'
      >>> v.vend()
      'You must deposit $3 more.'
      >>> v.deposit(5)
      'Current balance: $12'
      >>> v.vend()
      'Here is your candy and $2 change.'
      >>> v.deposit(10)
      'Current balance: $10'
      >>> v.vend()
      'Here is your candy.'
      >>> v.deposit(15)
      'Machine is out of stock. Here is your $15.'
      >>> w = VendingMachine('soda', 2)
      >>> w.restock(3)
      'Current soda stock: 3'
      >>> w.restock(3)
      'Current soda stock: 6'
      >>> w.deposit(2)
      'Current balance: $2'
      >>> w.vend()
      'Here is your soda.'
      """
      def __init__(self, product, price):
          self.product = product
          self.price = price
          self.balance = 0
          self.quantity = 0
      def vend(self):
          if self.quantity == 0:
              return "Machine is out of stock."
          elif self.balance < self.price:
              return f"You must deposit ${self.price - self.balance} more."
          elif self.balance > self.price:
              self.quantity = self.quantity - 1
              change = self.balance - self.price
              self.balance = self.balance - self.price - change
              return f"Here is your {self.product} and ${change} change."
          elif self.balance == self.price:
              self.quantity = self.quantity - 1
              self.balance = self.balance - self.price
              return f"Here is your {self.product}."
      def deposit(self, amount):
          if self.quantity <= 0:
              return f"Machine is out of stock. Here is your ${amount}."
          self.balance = self.balance + amount
          return f"Current balance: ${self.balance}"
      def restock(self, quant):
          self.quantity = self.quantity + quant
          return f"Current {self.product} stock: {self.quantity}"

v = VendingMachine('candy', 10)
#print(v.vend())
#print(v.deposit(15))
#print(v.restock(2))
#print(v.vend())
#print(v.deposit(7))
#print(v.vend())
#print(v.deposit(5))
#print(v.vend())
#print(v.deposit(10))
#print(v.vend())
#print(v.deposit(15))

#print()
w = VendingMachine('soda', 2)
#print(w.restock(3))
#print(w.restock(3))
#print(w.deposit(2))
#print(w.vend())


######################################## 4

class Manners:
      """
      A container class that only forwards messages that say please.
      >>> v = VendingMachine('teaspoon', 10)
      >>> v.restock(2)
      'Current teaspoon stock: 2'
      >>> m = Manners(v)
      >>> m.ask('vend')
      'You must learn to say please first.'
      >>> m.ask('please vend')
      'You must deposit $10 more.'
      >>> m.ask('please deposit', 20)
      'Current balance: $20'
      >>> m.ask('now will you vend?')
      'You must learn to say please first.'
      >>> m.ask('please hand over a teaspoon')
      'Thanks for asking, but I know not how to hand over a teaspoon.'
      >>> m.ask('please vend')
      'Here is your teaspoon and $10 change.'
      >>> double_fussy = Manners(m) # Composed Manners objects
      >>> double_fussy.ask('deposit', 10)
      'You must learn to say please first.'
      >>> double_fussy.ask('please deposit', 10)
      'Thanks for asking, but I know not how to deposit.'
      >>> double_fussy.ask('please please deposit', 10)
      'Thanks for asking, but I know not how to please deposit.'
      >>> double_fussy.ask('please ask', 'please deposit', 10)
      'Current balance: $10'
      """
      def __init__(self, obj):
          self.obj = obj
      def ask(self, message, *args):
          magic_word = 'please '
          if not message.startswith(magic_word):
              return 'You must learn to say please first.'
          request=message[7:]
          if request=='ask':
            try:
              return self.obj.ask(*args)
            except AttributeError:
              return 'Thanks for asking, but I know not how to '+request+'.'
          elif request=='deposit':
            try:
              return self.obj.deposit(*args)
            except AttributeError:
              return 'Thanks for asking, but I know not how to '+request+'.'
          elif request=='vend':
            try:
              return self.obj.vend()
            except AttributeError:
              return 'Thanks for asking, but I know not how to '+request+'.'
          else:
            return 'Thanks for asking, but I know not how to '+request+'.'


v = VendingMachine('teaspoon', 10)
v.restock(2)

m = Manners(v)
#print(m.ask('vend'))
#print(m.ask('please vend'))
#print(m.ask('please deposit', 20))
#print(m.ask('now will you vend?'))
#print(m.ask('please hand over a teaspoon'))
#print(m.ask('please vend'))

#print()

double_fussy = Manners(m) # Composed Manners objects
#print(double_fussy.ask('deposit', 10))
#print(double_fussy.ask('please vend'))
#print(double_fussy.ask('please please deposit', 10))
#print(double_fussy.ask('please ask', 'please deposit', 10))



######################################## LAB REPORT

class Car(object):
      num_wheels = 4
      gas = 30
      headlights = 2
      size = 'Tiny'
      def __init__(self, make, model):
          self.make = make
          self.model = model
          self.color = 'No color yet. You need to paint me.'
          self.wheels = Car.num_wheels
          self.gas = Car.gas
      def paint(self, color):
          self.color = color
          return self.make + ' ' + self.model + ' is now ' + color
      def drive(self):
          if self.wheels < Car.num_wheels or self.gas <= 0:
              return self.make + ' ' + self.model + ' cannot drive!'
          self.gas -= 10
          return self.make + ' ' + self.model + ' goes vroom!'


      def pop_tire(self):
        if self.wheels > 0:
            self.wheels -= 1

      def fill_gas(self):
          self.gas += 20
          return self.make + ' ' + self.model + ' gas level: ' + str(self.gas)

class MonsterTruck(Car):
    size = 'Monster'
    def rev(self):
        print('Vroom! This Monster Truck is huge!')
    def drive(self):
        self.rev()
        return Car.drive(self)


################ a.
#fancy_car = Car('Tesla', 'Model S')
# fancy_car.color

# This will return "No color yet, you need to paint me". The default value of the self.color is "No color yet, you need to paint me"
# when the method paint() is used then the color change

# --------

# fancy_car.paint('black')
# this will return: Tesla Model S is not black
# this is because the method paint changes the color and then return that string

# --------

# fancy_car.color
# when the paint color was called the value of color changed to black
# so the color attribute is displayed it will return black


################ b.

# >>> fancy_car = Car(’Tesla’, ’Model S’)
# >>> fancy_truck = MonsterTruck(’Monster Truck’, ’XXL’)
# >>> fancy_car.size

# the Car class has a class variable called size which is set to "Tiny"
# so when the object fancy_car calls the attibute size: it returns "Tiny"

# --------

# >>> fancy_truck.size
# fancy_truck is an object of the class MonsterTruck but that class inherits from the class Car
# so since it has not been changed, whenever the fancy_truck.size is called, it will also return "Tiny"


################ c.

fancy_car = Car('Tesla', 'Model S')
# fancy_car.model

# this is calling the attribute model which is passed to the init method
# it returns Model S

# --------

# fancy_car.gas = 10
# fancy_car.drive()


# gas was a class variable that was initially set to 30 but for this specific instance it's been set to 10
#  then the method drive() is called that takes 10 from gas and returns
# Tesla Model S goes vroom!

# --------

# >>> fancy_car.drive()
# since gas is now 0, it will then return Tesla Model S cannot drive

# --------

# >>> fancy_car.fill_gas()
# this adds 20 to the gas attribute and
# now returns: Tesla Model S gas level: 20

# --------

# >>> fancy_car.gas
# since the gas was filled, this then returns 20

# --------

# Car.gas
# this is not referring to the specific instance so this is still referencing to the
# class variable which is 30


################ d.


# >>> Car.headlights
# this references to the class attribute which is set to 2

# --------

# >>> fancy_car.headlights
# this references to the class attribute which is set to 2

# --------

# Car.headlights = 3
# fancy_car.headlights

# the class attribute headligths was changed to 3 so each instance of the class will have headlights with a value of 3

# --------

# >>> fancy_car.headlights = 2
# >>> Car.headlights

# this will return 3 because the headlights attribute was changed only on a specific instance of the class (fancy_car)
# however the attribute remains the same in the class


################ e.

# >>> fancy_car.wheels = 2
# >>> fancy_car.wheels

# it returns 2 because the attribute was previously set to two

#>>> Car.num_wheels

# it returns 4, it takes it from the class attribute
#   ______

# >>> ancy_car.drive()
# Tesla Model S goes vroom!

#______

#>>> Car.drive()

# it returns Tesla Model S goes vroom!
# gas is equal to 20 now
#______

# >>> Car.drive(fancy_car)
# it returns Tesla Model S goes vroom!

#______
#>>> MonsterTruck.drive(fancy_car)
# it primts Vroom! This Monster Truck is huge! to the output screen

###########f.
#>>> monster_jam = MonsterTruck(’Monster’, ’Batmobile’) 
#>>> monster_jam.drive()

#it first prints this since it refers to the drive method in MonsterTruck: Vroom! This Monster Truck is huge!
#but that method also returns the drive method in car which returns: Monster Batmobile goes vroom!

#______

#>>> Car.drive(monster_jam)
#Monster Batmobile goes vroom!

#______

#>>> MonsterTruck.drive(monster_jam)
#it first prints this since it refers to the drive method in MonsterTruck: Vroom! This Monster Truck is huge!
#but that method also returns the drive method in car which returns: Monster Batmobile goes vroom!

#______
#>>> Car.rev(monster_jam)
# this gives an error since the class Car does not have a rev method. For this to work it should be MosterTruck.rev(monster_jam)

#######g.
class FoodTruck(MonsterTruck):
   delicious = 'meh'
   def serve(self):
       if FoodTruck.size == 'delicious':
           print('Yum!')
       if self.food != 'Tacos':
           return 'But no tacos...'
       else:
           return 'Mmm!'
#>>> taco_truck = FoodTruck(’Tacos’, ’Truck’)
#>>>taco_truck.food = ’Guacamole’
#>>>taco_truck.serve()

# this returns But no tacos...

#______

#>>> taco_truck.food = taco_truck.make
#>>> FoodTruck.size = taco_truck.delicious
#>>> taco_truck.serve()

# this will retur Mmm!

#______

#>>> taco_truck.size = ’delicious’
#>>> taco_truck.serve()

# this again returns Mmm!

#______

#>>> FoodTruck.pop_tire()

# this call is missing a specific object its calling upon, an instance of FoodTruck should be in the parenthesis 

#______

#>>> FoodTruck.pop_tire(taco_truck)

#this refers back to pop tire method in Car class it doesnt return anything but it subtracts one from its wheels attribute

#______

#>>> taco_truck.drive()
# it first prints this since it refers to the drive method in MonsterTruck: Vroom! This Monster Truck is huge!
# but that method also returns the drive method in car which returns: Tacos Truck cannot drive!

#______


######################################## 6
from datetime import datetime, timedelta
import math
class ConstructionSite:
    '''
    >>> myConstructionSite=ConstructionSite('Commercial',2022,4,2,20)
    >>> myDumpTruck=DumpTruck()
    >>> myConstructionSite.addInVehicle(myDumpTruck)
    >>> myDumpTruck.doingRightNow()
    The  dumptruck  is  standing  at  (0, 0)
    >>> myCrane=Crane()
    >>> myConstructionSite.addInVehicle(myCrane)
    >>> myConstructionSite.vehicles[1].doingRightNow()
    The  crane  is  standing  at  (0, 0)  in  north
    >>> myConstructionSite.vehicles[1].rotate(2)
    >>> myConstructionSite.vehicles[1].doingRightNow()
    The  crane  is  standing  at  (0, 0)  in  south
    >>> myConstructionSite.vehicles[0].move(1,2)
    >>> myConstructionSite.vehicles[0].position
    (1, 2)
    '''
    def __init__(self, type, year,month,day, durationInDays):
        self.type=type #residential,commercial, transport, etc.
        self.startDate=datetime(year,month,day)
        self.endDate=self.startDate+timedelta(days=durationInDays)
        self.vehicles=[]
    def addInVehicle(self,obj):
        self.vehicles.append(obj)
class ConstructionVehicle(ConstructionSite):
    def __init__(self):
        self.position=(0,0)
        self.state='standing'
    def doingRightNow(self):
        print('The ',self.type, ' is ',self.state,' at ',self.position)
    def move(self,x,y):
        self.position=(self.position[0]+x,self.position[1]+y)
class DumpTruck(ConstructionVehicle):
    type='dumptruck'
    def deliver(self,position):
        self.position=position
        self.state='delivering'
    def load(self,position):
        self.position=position
        self.state='loading'
class Excavator(ConstructionVehicle):
    type='excavator'
    def Dig(self,position):
        self.position=position
        self.state='digging'
class Crane(ConstructionVehicle):
    type='crane'
    def __init__(self):
        ConstructionVehicle.__init__(self)
        self.direction='north'
    def liftUp(self):
        self.state='lifting'
    def putDown(self):
        self.state='putting down'
    def rotate(self, numberOfQuarters):
        lst=['north','east','south','west']
        while lst[0]!=self.direction:
            first=numberOfQuarters[0]
            numberOfQuarters=numberOfQuarters[1:].append(first)
        if numberOfQuarters>3:
            if numberOfQuarters%4==0:
                x=math.floor(numberOfQuarters/4)
                numberOfQuarters=numberOfQuarters - x*4
            else:
                numberOfQuarters=0
        self.direction=lst[numberOfQuarters]
    def doingRightNow(self):
        print('The ',self.type, ' is ',self.state,' at ',self.position,' in ',self.direction)


# Our parent class is ConstructionSite which includes a type attribute (residential, commercial, transport etc) it takes in the year, month 
# and day of today and a numberOfDuration and uses that to set a startDate attribute and a endDate attribute. We decided to focus on just vehicles
# in our abstraction, first we included safety measures such as fences and cones as well but as they dont really do anything they didn't add much.
# Therefore we included a vehicles attributes which is a list (empty list at first), and the class has a addInVehicle method that adds vehicles to
# the site. Then we added a class as a child of the ConstructionSite class that is ConstructionVehicle which is initiated to have a attribute position be in an entering
# (origin) and a state ('standing') it has 2 methods which are doingRightNow which prints a string explaining what a particular vehicle is doing right
# and it has a move method that changes the position of particular vehicle. ConstructionVehicle has 3 children: DumpTruck, Excavator and Crane. They
# all have a class attribute type that contains their name in a string (this is used in doingRightNow method). DumpTruck has 2 methods: deliver and load
# that take changes the state of the vehicle to that action and its position to the position where it is ordered to do it. The Excavator has a dig
# that works the same. Then the Crane class is a little different since a Crane often stays at the same place its methods, liftUp, putDown, and rotate
# do not take in a position but only change the state of the vehicle. However, a Crane does have an extra attribute direction which specifies in what 
# direction the crane is pointing. The rotate method takes in a number of quarters it has to rotate and changes its direction accordingly.
# Please see the docstring for some examples!


testmod()
