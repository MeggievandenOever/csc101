'''1) Write a function a plus abs b that adds a to the absolute value of b
without calling abs'''

def a_plus_abs_b(a,b):
  if b>=0:
    return b+a
  else:
    return -1*b+a
print(a_plus_abs_b(2,3))
print(a_plus_abs_b(2,-3))
print(a_plus_abs_b(-2,3))
print(a_plus_abs_b(-2,-3))

#2#######################################################
def two_of_three(x,y,z):
 if z==min(x,y,z):
   return (x**2)+(y**2)
 elif y==min(x,y,z):
   return (x**2)+(z**2)
 else:
   return (y**2)+(z**2)
lin=input("input 3 integers with no commas  ")
lin=lin.split()
x,y,z=int(lin[0]),int(lin[1]),int(lin[2])
print(two_of_three(x,y,z))

#3
import sys
m=int(input())
def sum(n):
    count=n-1
    while n%count !=0:
        count=count-1
    return print(count)
sum(m)

'''4'''
def c():
  return False
def t():
  print(1)
def f():
  print(2)

def if_function(condition, true_result, false_result):
  if condition:
    return true_result
  else:
    return false_result

def with_if_statement():
  if c():
    return t()
  else:
    return f()

def with_if_function():
  return if_function(c(), t(), f())

print(with_if_statement())
print(with_if_function())

#5#############################
def hailstone(x):
 if x==1:
   return print(x)
 elif x%2==0:
   return print(int(x)), hailstone(int(x/2))
 else:
   return print(int(x)), hailstone(int(x*3+1))
hailstone(int(input("Input an integer ")))

'''6) Write a function hm_string_split which takes a string and splits it
on the white spaces without using the split() method'''

def make_new_string(string):
  string = string[1:]
  count = 0
  newstring = ''
  while count < len(string):
    if string[count-1]==' ':
      if newstring[-1]==' ':
        newstring = newstring[:-1:]
      return [newstring,count]
    else:
      count = count+1
      newstring = newstring + string[count-1]
  if newstring[-1]==' ':
      newstring = newstring[:-1:]
  return [newstring,count]

def hm_string_split(string):
  count = 0
  list = []
  listcount = 0
  astring = ''
  while count < len(string):
    if string[count]==' ':
      list.append(astring)
      astring =''
      newstring, newcount = make_new_string(string[count:])
      list.append(newstring)
      count = count + (newcount + 1)
      print(str(count))
    else:
      astring = astring + string[count]
      count = count +1
  if not astring == '':
    list.append(astring)
  return list

print(hm_string_split('hello hoi hey'))
print(hm_string_split('string split'))
print(hm_string_split(''))

'''8) Explain the distinction between the if statement and if function, demonstrated in question 4.
So we have the following three functions:'''
def c():
  return False
def t():
  print(1)
def f():
  print(2)
'''
Now without looking at the if_statement() and if_function(), consider the following function:'''
def bla(x):
  print('hello')
'''this function takes in a variable x, however it doesn't use it but does print 'hello'. So what happends is the following:
Globally in bla(), x is set equal to whatever it is given and then hello is printed. Now consider the following function call:'''
bla(f())
'''So bla() is called and the first thing that happens is x is set equal to the variable it has been given so x = f(), so now locally 
f() is called and thus print(2) is executed and so 2 is printed to the output screen. Now the bla function continues and hello is printed
to the screen. So 2 and hello are printed to the screen. This exactly happens when you call if_function(c(),t(),f()). So the first things
that happens is that condition is set to c(). c() has a return value of False and thus condition is set to False. Then true_result is set 
equal to t(), so then t() is called and executes print(1) and so 1 is printed to the screen. Then false_result is set equal to f(), so 
then f() is called and executes print(2) and thus 2 is also printed to the screen. However nothing is returned, only printed, so return
value is the same as the if_statement and is None. Moreover, it has everything to do with the parameters of the if_function.
'''

#9#######################################################
#a will print the following
#print(xk(10, 10)) will return 23. this is because it will run through each test and see that c is not 4 so it will go down to the elif condition and see that 10 is larger than 4 and will return 6+7+10 which is 23
#print(xk(10, 6)) will return 23 as 10 is not equal to four it goes down to the elif section. because 6 is greater than 4 it adds 6+7+10
#print(xk(4, 6)) will return 6 as c is equal to four and all it must do is print 6
#print(xk(0, 0)) will return 25 because 0 is not equal to four and 0 is not greater than four it retunrs the else condition
########################################
#b will print 2,1,0,-1 as the print fnction is inside the while loop. so each time the loop will print the new n value as long as n is greater/equal to zero
###########################################
#C will produce infinitely many positive? strings as this while loop just states if the variable then this without a stopping condition
###########################################
#D will print -12,-9,-6 because it is testing to see while our function is negative and if it is positive printing negative as we add 3 more to each
###########################################
#e
#bake(0, 29) will print 1 29 as it will check if cake = 0, it does so it adds 1 to cake and prints it. cake now is assigned one and the function reads the next line that says cak is 1, this is true again so it will now print 29
#bake(1, "mashed potatoes") will print mashed potatoes as it will see cake is equal to one it will print make which is mashed potatoes

####ASSESSMENT OF WORK################
#the work was divided evenly through out the group. 
#Meggie did problems 1 4 6 and 8
#Emyle did problems 2 5 9
#Gibbs did problems 3 7 