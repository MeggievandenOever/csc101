#1a) string to binary digets
def bin_dig(x):
    return 0*int(x==0)+int(x=='1')
print(bin_dig('0'))

#1b) string to base 10 digets 
def base10_dig(x):
    return 0*int(x=="0")+int(x=="1")+2*int(x=="2")+3*int(x=="3")+4*int(x=="4")+5*int(x=="5")+6*int(x=="6")+7*int(x=="7")+8*int(x=="8")+9*int(x=="9")

print(base10_dig('0'))
print(base10_dig('1'))
print(base10_dig('5'))
print(base10_dig('9'))

#1c) string to hexadecimal digits 
def hexadec_dig(x):
  return 0*int(x=="0")+int(x=="1")+2*int(x=='2')+3*int(x=='3')+4*int(x=='4')+5*int(x=='5')+6*int(x=='6')+7*int(x=='7')+8*int(x=='8')+9*int(x=='9')
  +10*int(x=='A')+11*int(x=='B')+12*int(x=='C')+13*int(x=='D')+14*int(x=='E')+15*int(x=='F')
print(hexadec_dig('9'))
print(hexadec_dig('D'))

#2a
def binary2(x):
 return((bin_dig(x[0])*2**7)+(bin_dig(x[1])*2**6)+(bin_dig(x[2])*2**5)+(bin_dig(x[3])*2**4)+(bin_dig(x[4])*2**3)+(bin_dig(x[5])*2**2)+(bin_dig(x[6])*2**1)+(bin_dig(x[7])*2**0))
print(binary2('10010001'))

#2b
def dec1(x):
    return((base10_dig(x[0])*100)+(base10_dig(x[1])*10)+(base10_dig(x[2]*1)))
print(dec1('145'))

#2c (uses hexadec_dig from ex 1c)
def hexdec1(x):
    return((hexadec_dig(x[0])*16)+(hexadec_dig(x[1]*1)))
print(hexdec1('91'))

#3a) binary bit to string 
def rev_bin_dig(x):
  return "% s"%x
print(rev_bin_dig(0))
print(type(rev_bin_dig(0)))
print(rev_bin_dig(1))

#3b) base 10 digit to string
def rev_base10_dig(x):
  return "% s"%x
print(rev_base10_dig(5))
print(type(rev_base10_dig(5)))

#3c) hexadecimal to string
def rev_hexadec_dig(x):
  return(x<=9)* ("% s"%x)+(x==10)*'A'+(x==11)*'B'+(x==12)*'C'+(x==13)*'D'+(x==14)*'E'+(x==15)*'F'
rev_hexadec_dig(15)

#4a 
def nums(x):
    a1= x%2
    x= x//2
    a2= x%2
    x= x//2
    a3= x%2
    x= x//2
    a4= x%2
    x= x//2
    a5= x%2
    x= x//2
    a6= x%2
    x= x//2
    a7= x%2
    x= x//2
    a8= x%2
    return str(a8)+str(a7)+str(a6)+str(a5)+str(a4)+str(a3)+str(a2)+str(a1)
print(type(nums(255)))
print(nums(255))

#4b
def sec(x):
    return str(x)
print(type(sec(145)))
print(sec(145))

#4c
def rev_hexadec_dig(x):
  return(x<=9)* ("% s"%x)+(x==10)*'A'+(x==11)*'B'+(x==12)*'C'+(x==13)*'D'+(x==14)*'E'+(x==15)*'F'
def base10_to_hex(x):
 z= rev_hexadec_dig(x//16)+rev_hexadec_dig(x%16)
 return z
print(base10_to_hex(145))
print(base10_to_hex(255))

#5a 
def eval_exp(x):
  return 8*int(x[0]=='1')+4*int(x[1]=='1') + 2*int(x[2]=='1')+int(x[3]=='1')
def move_dec(x):
  newx = x[7] + x[9:20]
  dec = eval_exp(x[23:27])
  return newx[0:dec]+'.'+newx[dec:]
def eval_int(x):
  return 2*int(x[0]=='1')+int(x[1]=='1') + 0.5*int(x[3]=='1')+0.25*int(x[4]=='1')+0.125*int(x[5]=='1')+0.0625*int(x[6]=='1')+0.03125*int(x[7]=='1')
def final(x):
  return eval_int(move_dec(x))
final('(-1)^0*1.10010001111*2^0010')

#5b)
import math
def int_to_base2(x):
  firstdig = str(x%2)
  x = math.floor(x/2)
  seconddig = str(x%2)
  return firstdig + seconddig

def dec_to_base2(x):
  firstdig = str(math.floor(x*2))
  x = x * 2 - math.floor(x*2)
  seconddig = str(math.floor(x*2))
  x = x * 2 - math.floor(x*2)
  thirddig = str(math.floor(x*2))
  x = x * 2 - math.floor(x*2)
  fourthdig = str(math.floor(x*2))
  x = x * 2 - math.floor(x*2)
  fifthdig = str(math.floor(x*2))
  x = x * 2 - math.floor(x*2)
  sixtdig = str(math.floor(x*2))
  x = x * 2 - math.floor(x*2)
  seventhdig = str(math.floor(x*2))
  x = x * 2 - math.floor(x*2)
  eightdig = str(math.floor(x*2))
  return  firstdig + seconddig + thirddig +fourthdig + fifthdig + sixtdig +seventhdig + eightdig

def base10_to_base2(x):
  begin = (x<0)*'(-1)^1'+(x>=0)*'(-1)^0' + '*'
  return begin + int_to_base2(math.floor(x)) + '.' +dec_to_base2(x - math.floor(x))
print(base10_to_base2(3.14))

